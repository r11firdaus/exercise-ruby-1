class Player
    attr_accessor :name, :blood, :manna

    def initialize(name)
        @name = name
        @blood = 100
        @manna = 40
    end
    
    def getData
        {name: @name, blood: @blood, manna: @manna}
    end
end

$arr = []
$playerAttack = ""

def matchDetail()
    for player in $arr do
        "#{player[:name]}: manna = #{player[:manna]}, blood = #{player[:blood]}"
    end
end

def newPlayer()
    puts "Put Player Name"
    askName = gets.chomp
    player = Player.new(askName)
    $arr.push(player.getData())
    home()
end

def fight()
    p "Hit Enter to Attack"
    puts "Player Attacking => #{$playerAttack}"
    ask = gets.chomp
    
    if ask
        if $playerAttack == $arr[0][:name]
            $arr[1][:blood] -= 20
            $playerAttack = $arr[1][:name]
        else
            $arr[0][:blood] -= 20
            $playerAttack = $arr[0][:name]
        end
        puts matchDetail()
        if $arr[0][:blood] <= 0 || $arr[1][:blood] <= 0
            puts "Results: "
            puts matchDetail()
        else
            fight()
        end
    end
end

def start()
    puts "Welcome to the Battle Arena"
    puts "Battle Start:"
    puts "Who Will attack: #{$arr[0][:name]}"
    puts "Who Attacked: #{$arr[1][:name]}"
    puts "Description:"
    puts matchDetail()
    $playerAttack = $arr[0][:name]
    fight()
end

def home()
    puts "Welcome to the Battle Arena"
    puts "Description: "
    puts "Type new to create a character " if $arr.length < 2
    puts "Type start to begin the fight "
    ask = gets.chomp
    
    if ask == "new"
        newPlayer()
    else
        start()
    end
end

home()